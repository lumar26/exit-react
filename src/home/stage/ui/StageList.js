import Box from "@mui/material/Box";
import {useStages} from "../core/stages.hooks";
import Stage from "./Stage";

const StageList = () => {
    const stages = useStages();

    if (!stages || stages.length === 0) return <Box>There are no Stages</Box>;

    return (
        <Box>
            {stages.map((stage) => (
                <Stage stage={stage}/>
            ))}
        </Box>
    );
};

export default StageList;
