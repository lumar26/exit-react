import Box from "@mui/material/Box";
import { useRoutes } from "react-router-dom";
import AuthPage from "./auth/ui/AuthPage";
import EventList from "./home/event/ui/EventList";
import EventsPage from "./home/event/ui/EventsPage";
import HomePage from "./home/HomePage";
import PerformerFilteredList from "./home/performer/ui/PerformerFilteredList";
import PerformerList from "./home/performer/ui/PerformerList";
import PerformersPage from "./home/performer/ui/PerformersPage";
import StageList from "./home/stage/ui/StageList";
import StagesPage from "./home/stage/ui/StagesPage";
import StageTile from "./home/stage/ui/StageTile";

const AppRouter = () => {
    return useRoutes([
        {
            path: "/login",
            element: <AuthPage />,
        },
        {
            path: "/",
            element: <HomePage />,
            children: [
                {
                    path: "stages",
                    element: <StagesPage />,
                    children: [
                        {
                            path: "list",
                            element: <StageList />,
                        },
                        {
                            path: ":stageId",
                            element: <StageTile />,
                        },
                    ],
                },
                {
                    path: "events",
                    element: <EventsPage />,
                    children: [
                        {
                            path: "list",
                            element: <EventList />,
                        },
                    ],
                },
                {
                    path: "performers",
                    element: <PerformersPage />,
                    children: [
                        {
                            path: "list",
                            element: <PerformerList />,
                        },
                        {
                            path: ":filter",
                            element: <PerformerFilteredList />,
                        },
                    ],
                },
            ],
        },
        { path: "*", element: <Box>NotFound</Box> },
    ]);
};

export default AppRouter;
