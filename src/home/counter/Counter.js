import {Box} from "@mui/material";
import {useEffect, useState} from "react";
import CounterField from "./CounterField";
import calculateCounters from "./CounterCalculator";

const startDate = new Date(2022, 7, 10);

const Counter = () => {
    const [currentDate, setCurrentDate] = useState(new Date());
    const counters = calculateCounters(currentDate, startDate);
    useEffect(() => {
        setTimeout(() => {
            setCurrentDate(new Date());
        }, 1000)
    }, [currentDate]);

    return (
        <Box
            style={{
                background: "linear-gradient(-45deg,#EE7752,#E73C7E,#23A6D5,#23D5AB)",
            }}
            sx={{
                p: 1,
                display: "flex",
            }}
        >
            {/*prosledjujemo svaki period i njegovu vrednost, i na osnovu njega vracamo listu CounterField-ova*/}
            {counters.map((field) => {
                return <CounterField period={field.period} value={field.value}/>;
            })}
        </Box>
    );
}

export default Counter;