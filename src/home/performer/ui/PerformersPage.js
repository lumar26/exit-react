import { Outlet } from "react-router-dom";

const PerformersPage = () => {
    return <Outlet />;
};

export default PerformersPage;
