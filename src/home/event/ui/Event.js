import { Card, CardContent, CardMedia } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";

const Event = ({ event }) => {
    const date = new Date(event.start);

    return (
        <Card
            sx={{ display: "flex", justifyContent: "space-between", my: 2 }}
            raised
        >
            <Box sx={{ display: "flex" }}>
                <CardMedia
                    component="img"
                    sx={{ width: 360, height: 200 }}
                    image={event.image}
                    alt="Event Image"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <CardContent sx={{ flex: "1 0 auto" }}>
                        <Typography component="div" variant="h5">
                            {event.name}
                        </Typography>
                        <Typography
                            variant="subtitle1"
                            color="text.secondary"
                            component="div"
                        >
                            <Link to={`/stages/${event.stage}`}>Stage</Link>
                        </Typography>
                        <Typography
                            variant="subtitle1"
                            color="text.secondary"
                            component="div"
                        >
                            <Link to={`/performers/${event.performers.join("-")}`}>
                                Performers
                            </Link>
                        </Typography>
                    </CardContent>
                </Box>
            </Box>
            <Box
                sx={{
                    margin: 5,
                    borderRadius: 1,
                    bgcolor: "secondary.light",
                    color: "white",
                    width: 200,
                    fontWeight: 600,
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Box>Date</Box>
                <Box>{date.toLocaleDateString()}</Box>
            </Box>
        </Card>
    );
};

export default Event;
