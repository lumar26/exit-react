import Container from "@mui/material/Container";
import {Outlet} from "react-router-dom";
import Counter from "./counter/Counter";
import Navbar from "./nav/Navbar";
import {Box} from "@mui/material";

const HomePage = () => {
    // const dispatch = useDispatch();

    return (
        <Container>
            <Counter/>
            <Navbar/>
            <Outlet/>

                <Box
                    component="img"
                    sx={{
                        display: "flex",
                        my: 10,
                        height: {
                            lg: 719.6,
                            md: 500,
                            sm: 350
                        },
                        width: {
                            lg: 1150,
                            md: 800,
                            sm: 560
                        },
                        borderRadius: 3
                    }}
                    alt="cover"
                    src="/images/cover.jpg"
                    raised
                />

        </Container>
    );
};

export default HomePage;
