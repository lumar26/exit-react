import * as React from "react";
import Container from "@mui/material/Container";
import Login from "./login/Login";
import SignUp from "./signup/SignUp";

const AuthPage = () => {
    return (
        <Container
            component="main"
            maxWidth="lg"
            sx={{
                mt: {
                    lg: "10%",
                },
                display: "flex",
                flexDirection: {
                    xs: "column",
                    lg: "row",
                },
                alignItems: {
                    xs: "center",
                    lg: "start",
                },
            }}
        >
            <Login />
            <SignUp />
        </Container>
    );
};

export default AuthPage;
