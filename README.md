# Exit-festival aplikacija
Domaći zadatak iz predmeta Internet tehnologije ([Katedra za elektronsko poslovanje](https://elab.fon.bg.ac.rs/), 
[Fakultet Organizacionaih Nauka](http://www.fon.bg.ac.rs/), [Univerzitet u Beogradu](https://www.bg.ac.rs/en/)) na temu:
Veb aplikacija Exit festival, koja imitira postojeću zvaničnu veb stranicu ovog festivala. 

Adresa originalnog veb sajta: [Exit festival](https://www.exitfest.org/en).

## Korišćene tehnologije

Ovaj projekat predstavlja front-end aplikaciju urađenu u ReactJS frjmovorku jezika JavaScript
Zvanična dokumentacija:  [ReactJS](https://reactjs.org/docs/getting-started.html).

Pored standardnih paketa koje sa sobom donosi instalacija ReactJS-a u ovom projektu korišćeni su još i:

1. `Redux` biblioteka koja omogućava definisanje centralnog skladišta podataka na nivou cele aplikacije i nudi veiliki broj funkcionalnosti za rad sa stanjem aplikacije. Redux rešava problem React context-a kada je potrebno stanje deliti među više komponenti. U okviru Redux-a korišćen je i `Redux toolkit` koji pruža funkcionalnosti za lakše pisanje Redux logike. [React Redux](https://react-redux.js.org/).
2. `Material UI` biblioteka koja pruža programeru veliki broj predefinisanoh React komponenti kao i mogućnosti za prilagođenu stilizaciju tih komponenti. [Material UI](https://mui.com/). 
3. `React Router` biblioteka čija je uloga da korisniku omogući da na jednostavan način upravlja rutiranjem i prusmeravanjem u oviru veb aplikacije. [React Router](https://reactrouter.com/).


## Opis aplikacije i uputstvo za korisnike
###Upitstvo za upotrebu
Aplikacija se može pokrenuti tako što korinsik klonira projekt sa GitLab adrese: [https://gitlab.com/lumar26/exit-react.git](https://gitlab.com/lumar26/exit-react.git)

**Napomena: korinsik na svojoj mašini sa koje pokreće aplikaciju mora da ima instaliran [Node Package Manager](https://nodejs.org/en/download/).**

Nakon toga potrebno je otvoriti terminal i postaviti se u direktorijum `~/.../exit-festival/` i pokrenuti aplikaciju komandom 
`npm start`. Nakon toga u pretraživaču će se otvoriti aplikacija u novoj kartici.

### Opis
Aplikacija pruža uvid u Exit festival 2022 godine. Korisnik pri pristupanju sajtu biva preusmeren na početnu
stranicu na kojoj su vidljivi brojač koji odbrojava dane, minute i sekunde do početka festivala odnosno do 
početka prvog događaja. 

Pored brojača korisniku je vidljiva i navigaciona traka na kojoj se nalaze stavke menija: 
`EXIT`, `Events`, `Stages`, `Performers`, `Contact` i u desnom kraju navigacione trake meni za rad sa
korisničkim nalogom gde korisnik može da se prijavi/registuje ili odjavi sa aplikacije ukoliko je već 
prijavljen.

Klikom na dugme EXIT korisnik se vraća na početnu stranicu.

Klikom na karticu `Events` korinsik se preusmerava na stranu koja mu prikazuje sve događaje koji su definisani za festival 
jedan ispod drugog. Svaki događaj, tj. `Event`  ima sliku, link ka listi izvođača koji u njemu učestvuju, link ka bini gde se događaj održava kao i datum početka.
Ukoliko korisnik želi da pogleda izvođače lkikom na link biće preusmeren na stranicu koja prikazuje isfiltrirane izvođače sa uslovom da učestvuju na datom događaju. 
Ukoliko odluči da pogleda binu biće preusmeren an stranicu gde će mu biti prikazana ta bina.

Klikom na karticu `Stages` korisnik se preusmerava na stranu koja prikazuje sve bine koje postoje na festivalu jednu ispod druge.
Svaki `Stage` je prikazan u vidu kartice koja na sebi ima fotografiju bine, njen naziv, lokaciju kao i kapacitet publike.

Ukoliko korisnik klikne na karticu `Performers` biće presumeren na stranu gde će mu se izlistati svi izvođači angažovani na ovogodišnjem festivalu
u vidu kartica koje su poređane jedna ispod druge i gde svaka `Performer` kartica sadrži fotografiju izvođača i osnovne podatke o njemu 
poput imena, prezimena i nadimka.

**Napomena: Funkcionalnosti vezane za prijavu, registraciju i odjavu korisnika nije implementirana**

##Karakteristični delovi koda
U ovom delu biće objašnjene neke od karakterističnih funkcionalnosti ove aplikacije.
###Counter komponenta
Na putanji `src/home/counter/Counter.js` nalazi se komponenta Counter u kojoj se najbolje vidi upotreba `useState` i `useEffect` udice.
Karakterističan deo koda:


    const [currentDate, setCurrentDate] = useState(new Date());
    const counters = calculateCounters(currentDate, startDate);
    useEffect(() => {
        setTimeout(() => {
            setCurrentDate(new Date());
        }, 1000)
    }, [currentDate]);
U ovom bloku koda vidimo da se pri renderovanju komponente definiše stanje `currentDate` koje predstavlja trenutni datum. 
Funkcije `calculateCounters` vraća vremenski period koji je ostao između trenutnog datuma i početka festivala.

Efekat koji je definisan u `useEffect` udici se okida svaki put kada dođe do promene stanja trenutnog datuma, a upravo
taj efekat i vrši tu promenu u razmaku od jedne sekunde pomoću funkcije `setTimeout`. U onom trenutku kad istekne ta jedna sekunda
poziva se setter funkcija koja ažurira stanje a pošto je to stanje drugi parametar useEffect udice znači da će se efekat ponovo okinuti nakon promene tog stanja.
###Rutiranje
U fajlu `AppRouter.js` se može videti način rutiranja u ovoj aplikaciji, koji ćemo objasniti na primeru jedne rute, `/stages/list`, koja se 
renderuje kad korisnik odabere karticu _Stages_:

    const AppRouter = () => {
        return useRoutes([
        {
            path: "/",
            element: <HomePage />,
            children: [
                {
                    path: "stages",
                    element: <StagesPage />,
                    children: [
                        {
                            path: "list",
                            element: <StageList />,
                        },
                        {
                        path: ":stageId",
                        element: <StageTile />,
                        },
            ],
    }, ...

Za definisanje putanji ovde se koristi udica `useRoutes` koja dolazi predefinisana sa paketom **React Router**.

U ovom fragmentu koda vidimo da se prilikom poyicioniranja na putanju `/` U veb brauyeru renderuje _HomePage_
stranica koja pomoću `<Outlet/>` taga vraća svoju decu, a to je _StagesList_ koja se nalazi na putanje `stages/list`.
Ista logika je primenjena i za sve ostale putanje u aplikaciji.

###Slice fajlovi
U sledećem primeru ćemo se pozabaviti jednim delom Redux biblioteke koji se bavi kreiranjem **Slice**
-ova. Slice predstavlja jedno "parče" centralnog skladišta koje Redux obezbeđuje, ali ne samo stanje u vidu
podataka već obuhvata i logiku za to stanje: akcije koje se izvršavaju and stanjem.


    import { createSlice } from "@reduxjs/toolkit";
    
    const initialState = [
        {
            id: 2,
            name: "Nick Cave and the Bad Seeds",
            start: "2022-07-20T20:00:00.000000Z",
            image: "/images/performers/nick-cave.jpg",
            stage: 3,
            performers: [1, 3, 5],
        },
    ];
    
    export const eventsSlice = createSlice({
        name: "events",
        initialState,
        reducers: {
                setEvents: (state , action) => {
                    console.log("akcija nad stanjem");
            },
        },
    });
    
    export const { setEvents } = eventsSlice.actions;
    
    export default eventsSlice.reducer;

U navedenom primeru se najpre uočava početno stanje aplikacije koje je definisano sa `initialState`
konstantom i sadrži jedan objekat koji predstavlja događaj.
Najbitniiji deo ovog fajla jeste `createSlice` udica koja obavlja sve potrebne operacije nad stanjem: definiše naziv tog dela stanja, 
postavlja početno stanje i na kraju postavlja Reducere-e, tj funkcije koje vrše promenu nad tim delom stanja.
Na kraju fajla uočavamo export bitnih stavki tj. akcija nad stanjem (setEvents) i Reducer-a.
###Event komponenta
Na primeru _Event_ komponente ćemo objasniti kako su pravljene reusable komponente i takođe korišćenje
**MaterialUI** biblioteke.
    const Event = ({ event }) => {
    const date = new Date(event.start);
    
        return (
            <Card
                sx={{ display: "flex", justifyContent: "space-between", my: 2 }}
                raised
            >
                <Box sx={{ display: "flex" }}>
                    <CardMedia
                        component="img"
                        sx={{ width: 360, height: 200 }}
                        image={event.image}
                        alt="Event Image"
                    />
                    <Box sx={{ display: "flex", flexDirection: "column" }}>
                        <CardContent sx={{ flex: "1 0 auto" }}>
                            <Typography component="div" variant="h5">
                                {event.name}
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="text.secondary"
                                component="div"
                            >
                                <Link to={`/stages/${event.stage}`}>Stage</Link>
                            </Typography>
                            <Typography
                                variant="subtitle1"
                                color="text.secondary"
                                component="div"
                            >
                                <Link to={`/performers/${event.performers.join("-")}`}>
                                    Performers
                                </Link>
                            </Typography>
                        </CardContent>
                    </Box>
                </Box>
                <Box
                    sx={{
                        margin: 5,
                        borderRadius: 1,
                        bgcolor: "secondary.light",
                        color: "white",
                        width: 200,
                        fontWeight: 600,
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Box>Date</Box>
                    <Box>{date.toLocaleDateString()}</Box>
                </Box>
            </Card>
        );
    };
Na datom primeru uočavamo mnoge predefnisane komponente koje nudi Material UI kao što su `<Card/>`,
`<Box/>`, `<CardMedia/>`, `<Link/>`,` <Typography/>`

`<Card/>` komponenta, kao što joj ime kaže služi za kreiranje komponenti u vidu kartica. Kao i svaku 
komponenta kartica se može stilizovati na licu mesta pristupanjem raznoraznim svojstvima koja su vezana za stilove.
U okviru Kartice definišemo `<CardMedia/>` komponentu unutar koje ćemo postaviti sliku i definisati njean svojstva, 
a takođe u okviru kartice možemo dodati i druge komponente kao što je `<Box/>`.

`<Box/>` komponenta je pandan `<div/>` komponenti u običnom HTML zapisu, i služi sličnoj nameni s tim što 
kao komponenta iz MUI biblioteke nudi dosta mogućnosti za prilagođavanje.

`<Link/>` komponenta služi za definisanje referenci na druge delove aplikacije i njoj se uvek prosleđuje
putanja u okviru svojstva **to={}**.

Još jedna komponenta je <Typography/>` koja služi da uokviri neki tekst i da se na osnovu predefinisane NUI teme 
vrši stilizacija teksta.