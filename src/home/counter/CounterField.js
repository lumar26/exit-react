import {Box} from "@mui/material";

const CounterField = ({period, value}) => {
    const values = value.split("");
    return (
        <Box
            key={period} // predstavlja tip perioda: dan, minut, sekunda
            sx={{
                mx: 1,
                display: "flex",
                flexDirection: "column",
            }}
        >
            {/*predstavlja */}
            <Box
                sx={{
                    color: "white",
                    textAlign: "center",
                    fontWeight: 600
                }}
            >
                {/*jedno polje sadrzi samo jedan period, a ispod njega vrednosti*/}
                {period}
            </Box>
            <Box
                sx={{
                    display: "flex",
                }}
            >
                <Box
                    sx={{
                        display: "flex",
                        justifySelf: "center"
                    }}
                >
                    {/*ispod naziva perioda na;azice se dve kartice sa vrednostima koje se stalno apdejtuju*/}
                    {values.map((number) => { //za svaku vrednost vracamo jedan Box koji sadrzi broj, jer je vrednost ustv broj
                        return (
                            <Box
                                sx={{
                                    mx: 0.5,
                                    color: "white",
                                    bgcolor: "error.main",
                                    borderRadius: 1,
                                    width: 18,
                                    height: 30,
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                            >
                                {number}
                            </Box>
                        );
                    })}
                </Box>
            </Box>
        </Box>
    );
}

export default CounterField;