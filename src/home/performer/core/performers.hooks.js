import { useSelector } from "react-redux";

export const usePerformers = () => useSelector((state) => state.performers);

// hook za vracanje izvodjaca filtriranih na osnovu id-ja
export const useFilteredPerformers = (filters) => {
    const performers = usePerformers();
    return performers.filter((performer) => filters.includes("" + performer.id));
};
