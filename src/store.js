import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./auth/core/user.slice";
import stagesReducer from "./home/stage/core/stages.slice";
import eventsReducer from "./home/event/core/events.slice";
import performerReducer from "./home/performer/core/performers.slice";

const store = configureStore({
    reducer: {
        user: userReducer,
        stages: stagesReducer,
        events: eventsReducer,
        performers: performerReducer,
    },
});

export default store;
