import { CssBaseline, ThemeProvider } from "@mui/material";
import { Provider } from "react-redux";
import AppRouter from "./AppRouter";
import theme from "./theme";
import store from "./store";

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline /> {/*mui*/}
            <Provider store={store}> {/*redux*/}
                <AppRouter />
            </Provider>
        </ThemeProvider>
    );
};

export default App;

