import { createSlice } from "@reduxjs/toolkit";

const initialState = [
    {
        id: 2,
        name: "Nick Cave and the Bad Seeds",
        start: "2022-07-20T20:00:00.000000Z",
        image: "/images/performers/nick-cave.jpg",
        stage: 3,
        performers: [1, 3, 5],
    },
    {
        id: 2,
        name: "Meduza haos",
        start: "2022-07-21T18:00:00.000000Z",
        image: "/images/performers/meduza.jpg",
        stage: 3,
        performers: [2, 4],
    },
];

export const eventsSlice = createSlice({
    name: "events",
    initialState,
    reducers: {
        setEvents: (state , action) => {
            console.log("akcija nad stanjem");
        },
    },
});

export const { setEvents } = eventsSlice.actions;

export default eventsSlice.reducer;
