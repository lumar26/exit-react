import { Card, CardContent, CardMedia } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

const Performer = ({ performer }) => {
    return (
        <Card
            sx={{ display: "flex", justifyContent: "space-between", my: 2 }}
            raised
        >
            <Box sx={{ display: "flex" }}>
                <CardMedia
                    component="img"
                    sx={{ width: 360, height: 200 }}
                    image={performer.image}
                    alt="Event Image"
                />
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <CardContent sx={{ flex: "1 0 auto" }}>
                        <Typography component="div" variant="h5">
                            {performer.nick}
                        </Typography>
                    </CardContent>
                </Box>
            </Box>
            <Box
                sx={{
                    margin: 5,
                    borderRadius: 1,
                    bgcolor: "secondary.light",
                    color: "white",
                    width: 200,
                    fontWeight: 600,
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Box>{performer.name}</Box>
                <Box>{performer.surname}</Box>
                <Box>{performer.music_genre}</Box>
            </Box>
        </Card>
    );
};

export default Performer;
