import Box from "@mui/material/Box";
import { usePerformers } from "../core/performers.hooks";
import Performer from "./Performer";

const PerformerList = () => {
    const performers = usePerformers();

    if (!performers)
        return <Box>There are no Performers</Box>;
    if (performers.length === 0)
        return <Box>List of performers empty</Box>

    return (
        <Box>
            {performers.map((performer) => (
                <Performer performer={performer} />
            ))}
        </Box>
    );
};

export default PerformerList;
