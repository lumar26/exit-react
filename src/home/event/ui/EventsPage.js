import { Outlet } from "react-router-dom";

const EventsPage = () => {
    return <Outlet />;
};

export default EventsPage;