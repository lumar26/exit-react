import { useSelector } from "react-redux";

export const useStages = () => useSelector((state) => state.stages);

export const useStage = (stageId) => {
    const stages = useStages();

    return stages.find((stage) => "" + stage.id === stageId);
};
