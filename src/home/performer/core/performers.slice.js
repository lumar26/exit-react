import { createSlice } from "@reduxjs/toolkit";

const initialState = [
    {
        id: 1,
        name: "Nicholas",
        surname: "Edvard Cave",
        image: "/images/performers/nick-cave.jpg",
        nick: "Nick Cave",
        music_genre: "Rock",
    },
    {
        id: 2,
        name: "Boris",
        surname: "Brejcha",
        image: "/images/performers/brejcha.jpg",
        nick: "Brejcha",
        music_genre: "Electronic Dance Music",
    },
    {
        id: 3,
        name: "Luca",
        surname: "de Gregorio",
        image: "/images/performers/meduza.jpg",
        nick: "Meduza",
        music_genre: "Electronic music",
    },
    {
        id: 4,
        name: "Sonny",
        surname: "John Moore",
        image: "/images/performers/skrillex.jpg",
        nick: "Skrillex",
        music_genre: "Electronic Dance Music",
    },
    {
        id: 5,
        name: "Mladen",
        surname: "Solomun",
        image: "/images/performers/solomun.jpg",
        nick: "Solomun",
        music_genre: "Electronic music",
    },
];

export const performersSlice = createSlice({
    name: "performers",
    initialState,
    reducers: {
        setPerformers: (state, action) => {
            state = [...action.payload];
        },
    },
});

export const { setPerformers } = performersSlice.actions;

export default performersSlice.reducer;
