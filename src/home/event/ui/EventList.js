import Box from "@mui/material/Box";
import useEvents from "../core/events.hooks";
import Event from "./Event";

const EventList = () => {
    const events = useEvents(); // dobijamo sve dogadjaje iz centralnog repo

    if (!events) return <Box>There are no Events</Box>

    return (
        <Box>
            {events.map((event) => {
                // vraca Card
                return <Event event={event} />;
            })}
        </Box>
    );
};

export default EventList;
