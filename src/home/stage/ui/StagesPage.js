import { Outlet } from "react-router-dom";

const StagesPage = () => {
    return <Outlet />;
};

export default StagesPage;
