import Box from "@mui/material/Box";
import { useParams } from "react-router-dom";
import { useFilteredPerformers } from "../core/performers.hooks";
import Performer from "./Performer";

const PerformerFilteredList = () => {
    const params = useParams();
    const { filter } = params;
    console.log(params);

    const performersIds = filter.split("-");
    console.log(performersIds)
    const performers = useFilteredPerformers(performersIds);
    console.log(performers)

    return (
        <Box>
            {performers.map((performer) => (
                <Performer performer={performer} />
            ))}
        </Box>
    );
};

export default PerformerFilteredList;
