import Box from "@mui/material/Box";
import { useParams } from "react-router-dom";
import { useStage } from "../core/stages.hooks";
import Stage from "./Stage";

const StageTile = () => {
    const { stageId } = useParams();
    const stage = useStage(stageId);

    if (!stage) return <Box>There is no Stage</Box>;

    return <Stage stage={stage} />;
};

export default StageTile
