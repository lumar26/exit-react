import { useSelector } from "react-redux";

// hook za pristup svim dogadjajima iz Central data store, samo radi lepseg koda da se zna da koristim events samo
const useEvents = () => useSelector((state) => state.events);

export default useEvents;
