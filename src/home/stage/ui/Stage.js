import { Card, CardContent, CardMedia } from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import PriceCheckIcon from '@mui/icons-material/PriceCheck';

const Stage = ({ stage }) => {
    return (
        <Card
            sx={{ display: "flex", justifyContent: "space-between", my: 2 }}
            raised
        >
            <Box sx={{ display: "flex" }}>
                <CardMedia
                    component="img"
                    sx={{ width: 360, height: 200 }}
                    image={stage.image}
                    alt="Event Image"
                />
                <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "space-around" }}>
                    <CardContent>
                        <Typography component="div" variant="h5">
                            {stage.name}
                        </Typography>
                        <Box>
                            <LocationOnIcon />
                            {stage.location}
                        </Box>
                        <Box> <PriceCheckIcon />{stage.sponsor}</Box>
                    </CardContent>
                </Box>
            </Box>
            <Box
                sx={{
                    margin: 5,
                    borderRadius: 1,
                    bgcolor: "secondary.light",
                    color: "white",
                    width: 200,
                    fontWeight: 600,
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <Box>Capacity</Box>
                <Box>{stage.capacity}</Box>
            </Box>
        </Card>
    );
};

export default Stage;
