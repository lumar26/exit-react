import { createSlice } from "@reduxjs/toolkit";

const initialState = [
    {
        id: 1,
        name: "Main Stage",
        image: "/images/stages/main-stage.jpg",
        location: "Petrovaradin",
        sponsor: "Exit",
        capacity: 11991,
    },
    {
        id: 2,
        name: "Dance Arena",
        image: "/images/stages/mts-dance-arena.jpg",
        location: "Petrovaradin",
        sponsor: "mts",
        capacity: 8046,
    },
    {
        id: 3,
        name: "Fusion Stage",
        image: "/images/stages/visa-fusion-stage.jpg",
        location: "Petrovaradin",
        sponsor: "VISA",
        capacity: 7332,
    },
    {
        id: 4,
        name: "Explosive Stage",
        image: "/images/stages/explosive-pub.jpg",
        location: "Petrovaradin",
        sponsor: "Explosive pub",
        capacity: 7306,
    },
    {
        id: 5,
        name: "No Sleep Stage",
        image: "/images/stages/no-sleep.jpg",
        location: "Petrovaradin",
        sponsor: "Guarana",
        capacity: 14672,
    },
    {
        id: 6,
        name: "Gang Beats Stage",
        image: "/images/stages/cockta-beats.jpg",
        location: "Petrovaradin",
        sponsor: "Noizz",
        capacity: 12464,
    },
];

export const stagesSlice = createSlice({
    name: "stages",
    initialState,
    reducers: {
        setStages: (state, action) => {
            state = [...action.payload];
        },
    },
});

export const { setStages } = stagesSlice.actions;

export default stagesSlice.reducer;
