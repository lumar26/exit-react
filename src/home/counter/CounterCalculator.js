

// fja za izracunavanje koliko je ostalo do pocetka festivala
const calculateCounters = (currentDate, startDate) => {
    const day = 1000 * 60 * 60 * 24;
    const hour = 1000 * 60 * 60;
    const minute = 1000 * 60;
    const second = 1000;

    const leftMilliseconds = startDate - currentDate; // pri oduzimanju datuma ostaju milisekunde
    const leftDays = Math.floor(leftMilliseconds / day);
    const leftHours = Math.floor((leftMilliseconds % day) / hour);
    const leftMinutes = Math.floor(((leftMilliseconds % day) % hour) / minute);
    const leftSeconds = Math.floor(
        (((leftMilliseconds % day) % hour) % minute) / second
    );

    return [
        {
            period: "Days",
            value: leftDays.toString(),
        },
        {
            period: "Hours",
            value: leftHours < 10 ? "0" + leftHours : leftHours.toString(),
        },
        {
            period: "Minutes",
            value: leftMinutes < 10 ? "0" + leftMinutes : leftMinutes.toString(),
        },
        {
            period: "Seconds",
            value: leftSeconds < 10 ? "0" + leftSeconds : leftSeconds.toString(),
        },
    ];
}

export default calculateCounters;